package de.primeapi.varo.listeners;

import com.avaje.ebean.SqlUpdate;
import de.primeapi.varo.Varo;
import de.primeapi.varo.sql.SQLUtils;
import de.primeapi.varo.utils.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.server.ServerListPingEvent;

import java.util.UUID;

public class Listeners implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e){
        if(Data.frozen.contains(e.getPlayer().getUniqueId())){
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e){
        if(Data.frozen.contains(e.getWhoClicked().getUniqueId())){
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onCreeper(EntityExplodeEvent e){
        if(!Data.started) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e){
        if(!(e.getEntity() instanceof Player)){
            return;
        }
        VaroPlayer p = new VaroPlayer((Player) e.getEntity());


        if(Data.frozen.contains(p.getUuid())){
            e.setCancelled(true);
        }

        if(!Data.sessioning.containsKey(p.getUuid())){
            e.setCancelled(true);
        }

        if(!Data.started){
            e.setCancelled(true);
        }
    }



    @EventHandler
    public void onInvClose(InventoryCloseEvent e){
        VaroPlayer p = new VaroPlayer((Player) e.getPlayer());
        if(Data.invsee.containsKey(p.getUuid())){
            OfflineVaroPlayer t = new OfflineVaroPlayer(Data.invsee.get(p.getUuid()));
            t.updateInventory(InventorySerialization.itemStackArrayToBase64(e.getInventory().getContents()));
            p.sendSuccess(" Du hast das Inventar von §e" + t.getName() + "§7 überschrieben!");
        }
    }

     @EventHandler
    public void onPing(ServerListPingEvent e){
         UUID uuid = SQLUtils.getUUIDByIP(e.getAddress().getHostName());
         String motd1 = "§6" + Data.domain + " §8 x §e§l Varo-Projekt";
         String motd2 = "";

         if(!Data.started){
             motd2 = "§7Status: §c§lVorbereitung...";
         }else if(uuid == null){
             motd2 = "§7Lebende Spieler: §e" + SQLUtils.getAlivePlayers().size() + "§8/§c" + SQLUtils.getAllPlayers().size();
         }else {
             VaroPlayer p = new VaroPlayer(uuid);

             if (p.getTeam() == null) {
                 motd2 = "§7Lebende Spieler: §e" + SQLUtils.getAlivePlayers().size() + "§8/§c" + SQLUtils.getAllPlayers().size();
             }else if(p.getHealth() == 0){
                 motd2 = "§7Spielbereit in: §cGestorben!";
             }else if(p.getStrikes().size() >= Data.maxStrikes){
                 motd2 = "§7Spielbereit in: §cGesperrt!";
             }else {
                 if(Utils.getLowestSessionValue(p) > System.currentTimeMillis()){
                     motd2 = "§7Spielbereit in: §c" + Utils.getRemainingTimeShort(Utils.getLowestSessionValue(p));
                 }else {
                     motd2 = "§7Spielbereit in: §aJetzt!";
                 }
             }
         }

         e.setMotd(motd1 + "\n" + motd2);
     }



}
