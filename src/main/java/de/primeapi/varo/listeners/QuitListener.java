package de.primeapi.varo.listeners;

import de.primeapi.varo.utils.Data;
import de.primeapi.varo.utils.InventorySerialization;
import de.primeapi.varo.utils.VaroPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class QuitListener implements Listener {

    @EventHandler
    public void onQuit(PlayerQuitEvent e){
        VaroPlayer p = new VaroPlayer(e.getPlayer());

        if(Data.sessioning.containsKey(p.getUuid()) && p.getHealth() != 0.0){
            Player player = e.getPlayer();
            p.updateInventory(InventorySerialization.itemStackArrayToBase64(player.getInventory().getContents()));
            p.updateArmor(InventorySerialization.itemStackArrayToBase64(player.getInventory().getArmorContents()));
            p.updateHealth(player.getHealth());
            p.updateLocation(player.getLocation());
        }
    }

}
