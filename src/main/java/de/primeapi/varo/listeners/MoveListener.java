package de.primeapi.varo.listeners;

import de.primeapi.varo.utils.Data;
import de.primeapi.varo.utils.VaroPlayer;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class MoveListener implements Listener {

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent e){
        VaroPlayer p = new VaroPlayer(e.getPlayer());

        if(Data.frozen.contains(p.getUuid())){
            if(e.getFrom().getBlockX() != e.getTo().getBlockX() || e.getFrom().getBlockZ() != e.getTo().getBlockZ()){
                p.thePlayer().teleport(e.getFrom());
                Location location = new Location(e.getFrom().getWorld(), e.getFrom().getBlockX() + 0.5, e.getFrom().getBlockY(), e.getFrom().getBlockZ() + 0.5);
            }
        }
    }

}
