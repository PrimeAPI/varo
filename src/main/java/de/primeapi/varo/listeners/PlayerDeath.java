package de.primeapi.varo.listeners;

import de.primeapi.varo.config.Conf;
import de.primeapi.varo.utils.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.util.List;

public class PlayerDeath implements Listener {

    @EventHandler
    public void onDeath(PlayerDeathEvent e){
        VaroPlayer p = new VaroPlayer(e.getEntity().getUniqueId());
        if(Data.sessioning.containsKey(p.getUuid())){
            Player player = p.thePlayer();
            p.updateInventory(InventorySerialization.itemStackArrayToBase64(player.getInventory().getContents()));
            p.updateArmor(InventorySerialization.itemStackArrayToBase64(player.getInventory().getArmorContents()));
            p.updateHealth(0.0D);
            p.updateLocation(player.getLocation());
            Data.sessioning.remove(p.getUuid());
            p.thePlayer().getLocation().getWorld().strikeLightningEffect(p.thePlayer().getLocation());
            if(p.thePlayer().getKiller() == null) {
                e.setDeathMessage(Data.pr + " §8[§3" + p.getTeam() + "§8] §b" + p.getName() + " §7 ist §cgestorben§7!");
            }else {
                VaroPlayer k = new VaroPlayer(p.thePlayer().getKiller());
                e.setDeathMessage(Data.pr + " §8[§3" + p.getTeam() + "§8] §b" + p.getName() + " §7 wurde von §8[§3" + k.getTeam() + "§8] §b" + k.getName() + " §cgetötet§7!");
                k.addKill(p.getUuid());
            }
            p.getPlayer().kickPlayer("§4§l➤ §6§lVaro §7● §cAusschluss\n" +
                    "\n" +
                    "§cDu bist §c§ngestorben§c!\n" +
                    "\n" +
                    "§eVielen dank fürs mitspielen" + "\n" +
                    "\n" +
                    "§7Varo-Projekt: §e" + Data.domain + "\n" +
                    "§aSponsored by§7: §e" + Data.sponsor);

            if(Conf.cfg.getBoolean("settings.worldboarder.reduce.active")) {
                for (Player all : Bukkit.getOnlinePlayers()) {
                    all.hidePlayer(p.thePlayer());
                    all.sendMessage(Data.pr + "§c Die Worldboarder schrumpft um §e" + Data.boarderReduce + "§c reduziert!");
                }
                for (World world : Bukkit.getWorlds()) {
                    world.getWorldBorder().setSize(world.getWorldBorder().getSize() - Data.boarderReduce, Data.boarderReduce * 4);
                }
            }
            List<OfflineVaroPlayer> win = Utils.getWin();
            if(win != null){
                if(win.size() == 0){
                    p.getPlayer().kickPlayer("§4§l➤ §6§lVaro §7● §cKick\n" +
                            "\n" +
                            "§cVaro wurde beendet!\n" +
                            "\n" +
                            "§7Varo-Projekt: §e" + Data.domain + "\n" +
                            "§aSponsored by§7: §e" + Data.sponsor);
                    return;
                }else {
                    p.getPlayer().kickPlayer("§4§l➤ §6§lVaro §7● §cKick\n" +
                            "\n" +
                            "§cVaro ist geendet!\n" +
                            "§7Sieger: §e" + win.get(0).getName() + "§7 und §e" + win.get(1).getName() + "\n" +
                            "\n" +
                            "§7Varo-Projekt: §e" + Data.domain + "\n" +
                            "§aSponsored by§7: §e" + Data.sponsor);
                    return;
                }
            }

        }
    }

}
