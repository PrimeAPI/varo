package de.primeapi.varo.listeners;

import de.primeapi.varo.utils.Data;
import de.primeapi.varo.utils.Scoreboard;
import de.primeapi.varo.utils.Utils;
import de.primeapi.varo.utils.VaroPlayer;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.ServiceConfigurationError;

public class JoinListener implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e){
        VaroPlayer p = new VaroPlayer(e.getPlayer());
        e.setJoinMessage(null);


        Scoreboard.updateScore(true);

        if(Data.sessioning.containsKey(p.getUuid())){
            return;
        }
        p.updateIP(p.thePlayer().getAddress().getHostName());

        if(p.thePlayer().hasPermission("varo.admin") || p.thePlayer().isOp()){
            p.thePlayer().getInventory().clear();
            p.thePlayer().getInventory().setArmorContents(null);
            p.sendSuccess(" Du wurdest in den Vanish-Versetzt!");
            p.sM(" Starte deine Session mit /session");
            p.getPlayer().setGameMode(GameMode.SPECTATOR);
            Data.vanished.add(p.getUuid());
            for(Player all : Bukkit.getOnlinePlayers()){
                if(!all.hasPermission("varo.admin")){
                    all.hidePlayer(p.getPlayer());
                }
            }
            return;
        }else {
            Utils.startSession(p);
        }
    }

}
