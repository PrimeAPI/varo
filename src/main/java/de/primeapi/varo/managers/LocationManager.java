package de.primeapi.varo.managers;

import com.sun.prism.shader.DrawEllipse_Color_Loader;
import de.primeapi.varo.config.Conf;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;


public class LocationManager {

    public static void setLocation(String key, Location location){
        Conf.cfg.set("locations." + key + ".x", location.getX());
        Conf.cfg.set("locations." + key + ".y", location.getY());
        Conf.cfg.set("locations." + key + ".z", location.getZ());
        Conf.cfg.set("locations." + key + ".yaw", location.getYaw());
        Conf.cfg.set("locations." + key + ".pitch", location.getPitch());
        Conf.cfg.set("locations." + key + ".world", location.getWorld().getName());
        Conf.save();
    }

    public static Location getLocation(String key){
        return new Location(Bukkit.getWorld(Conf.cfg.getString("locations." + key + ".world"))
                , Conf.cfg.getDouble("locations." + key + ".x")
                , Conf.cfg.getDouble("locations." + key + ".y")
                , Conf.cfg.getDouble("locations." + key + ".z")
                , (float) Conf.cfg.getInt("locations." + key + ".yaw")
                , (float) Conf.cfg.getInt("locations." + key + ".pitch"));
    }

    public static List<Integer> usedSpawns = new ArrayList<>();

    public static Location getNextSpawnLocation(){
        int i = 1;
        boolean found = false;
        while (!found){
            if(usedSpawns.contains(i)){
                i++;
            }else {
                found = true;
            }
        }

        try {
            Location location = getLocation(String.valueOf(i));
            usedSpawns.add(i);
            return location;
        }catch (Exception ex){
            return null;
        }

    }

}
