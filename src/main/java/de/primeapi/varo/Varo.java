package de.primeapi.varo;

import de.primeapi.varo.commands.*;
import de.primeapi.varo.config.Conf;
import de.primeapi.varo.listeners.*;
import de.primeapi.varo.sql.MySQL;
import de.primeapi.varo.utils.Data;
import de.primeapi.varo.utils.OfflineVaroPlayer;
import de.primeapi.varo.utils.Scoreboard;
import de.primeapi.varo.utils.Utils;
import javafx.print.PageLayout;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class Varo extends JavaPlugin {


    private static Varo instance;

    public static Varo getInstance() {
        return instance;
    }

    private static ThreadPoolExecutor threadPoolExecutor;

    public static ThreadPoolExecutor getThreadPoolExecutor() {
        return threadPoolExecutor;
    }

    @Override
    public void onEnable() {
        instance = this;
        threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
        Conf.init();
        MySQL.connect();


        System.out.println("[Varo] Enabling Varo v" + getDescription().getVersion() + " by PrimeAPI");

        System.out.println("[Varo] Loading Configuration...");


        getCommand("setlocation").setExecutor(new SetLocationCommand());
        getCommand("session").setExecutor(new SessionCommand());
        getCommand("register").setExecutor(new RegisterCommand());
        getCommand("countdown").setExecutor(new CountdownCommand());
        getCommand("manage").setExecutor(new ManageCommand());
        getCommand("gamemode").setExecutor(new GamemodeCommand());
        getCommand("gm").setExecutor(new GamemodeCommand());
        getCommand("ping").setExecutor(new PingCommand());
        getCommand("tp").setExecutor(new TeleportCommand());
        getCommand("teleport").setExecutor(new TeleportCommand());
        getCommand("tphere").setExecutor(new TeleportHereCommand());
        getCommand("teleporthere").setExecutor(new TeleportHereCommand());
        getCommand("day").setExecutor(new DayCommand());
        getCommand("fly").setExecutor(new FlyCommand());
        getCommand("heal").setExecutor(new HealCommand());
        getCommand("sun").setExecutor(new SunCommand());
        getCommand("teams").setExecutor(new TeamsCommand());
        getCommand("reset").setExecutor(new ResetCommand());
        getCommand("strike").setExecutor(new StrikeCommand());

        Bukkit.getPluginManager().registerEvents(new JoinListener(), this);
        Bukkit.getPluginManager().registerEvents(new MoveListener(), this);
        Bukkit.getPluginManager().registerEvents(new QuitListener(), this);
        Bukkit.getPluginManager().registerEvents(new Listeners(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerDeath(), this);

        Data.started = Conf.cfg.getBoolean("settings.started");
        if(Data.started){
            for(World world : Bukkit.getWorlds()){
                world.setDifficulty(Difficulty.NORMAL);
            }
        }else {
            for(World world : Bukkit.getWorlds()){
                world.setDifficulty(Difficulty.PEACEFUL);
            }
        }

        startScheduler();
    }

    @Override
    public void onDisable() {
        MySQL.disconnect();

        for(Player all : Bukkit.getOnlinePlayers()){
            all.kickPlayer("§4§l➤ §6§lVaro §7● §cKick\n" +
                    "\n" +
                    "§cDeine Session musste unerwartet beendet werden\n" +
                    "\n" +
                    "§cBitte melde dich bei einem Admin\n" +
                    "§7Varo-Projekt: §e" + Data.domain + "\n" +
                    "§aSponsored by§7: §e" + Data.sponsor);
        }
    }

    private void startScheduler(){
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> {

            for(UUID uuid : Data.sessioning.keySet()){
                int i = Data.sessioning.get(uuid);
                i--;
                Data.sessioning.put(uuid, i);
                String s = null;
                OfflineVaroPlayer p = new OfflineVaroPlayer(uuid);
                if(i == 60){
                    s = Data.pr + " Die session von §8[§3" + p.getTeam() + "§8] §b" + p.getName() + " §b" + p.getName() + " §7ended in §e60 Sekunden§7!";
                }
                if(i == 30){
                    s = Data.pr + " Die session von §8[§3" + p.getTeam() + "§8] §b" + p.getName() + " §7ended in §e30 Sekunden§7!";
                }
                if(i == 10){
                    s = Data.pr + " Die session von §8[§3" + p.getTeam() + "§8] §b" + p.getName() + " §7ended in §e10 Sekunden§7!";
                }
                if(i == 5){
                    s = Data.pr + " Die session von §8[§3" + p.getTeam() + "§8] §b" + p.getName() + " §7ended in §e2 Sekunden§7!";
                }
                if(i == 4){
                    s = Data.pr + " Die session von §8[§3" + p.getTeam() + "§8] §b" + p.getName() + " §7ended in §e4 Sekunden§7!";
                }
                if(i == 3){
                    s = Data.pr + " Die session von §8[§3" + p.getTeam() + "§8] §b" + p.getName() + " §7ended in §e3 Sekunden§7!";
                }
                if(i == 2){
                    s = Data.pr + " Die session von §8[§3" + p.getTeam() + "§8] §b" + p.getName() + " §7ended in §e2 Sekunden§7!";
                }
                if(i == 1){
                    s = Data.pr + " Die session von §8[§3" + p.getTeam() + "§8] §b" + p.getName() + " §7ended in §e1 Sekunden§7!";
                }
                if(i == 0){
                    s = Data.pr + " Die session von §8[§3" + p.getTeam() + "§8] §b" + p.getName() + " §7ended §cjetzt§7!";
                    if(Bukkit.getPlayer(uuid) != null){
                        Bukkit.getPlayer(uuid).kickPlayer("§4§l➤ §6§lVaro §7● §cKick\n" +
                                "\n" +
                                "§cDeine Session ist §c§nvorbei§c!\n" +
                                "\n" +
                                "\n" +
                                "§7Varo-Projekt: §e" + Data.domain + "\n" +
                                "§aSponsored by§7: §e" + Data.sponsor);
                    }
                    int sess = Utils.getLowestSession(p);
                    if(sess == 1){
                        p.updateSession1(System.currentTimeMillis() + Data.sessionWait);
                    }
                    if(sess == 2){
                        p.updateSession2(System.currentTimeMillis() + Data.sessionWait);
                    }
                    if(sess == 3){
                        p.updateSession3(System.currentTimeMillis() + Data.sessionWait);
                    }
                    Data.sessioning.remove(uuid);
                }
            }
            if(Data.started) {
                Scoreboard.updateScore();
            }
        }, 20L, 20L);
    }



}
