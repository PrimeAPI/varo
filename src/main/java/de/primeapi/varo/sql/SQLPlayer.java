package de.primeapi.varo.sql;


import com.sun.rmi.rmid.ExecPermission;
import de.primeapi.varo.utils.OfflineVaroPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class SQLPlayer {

    UUID uuid;
    String name;
    List<UUID> kills;
    List<String> strikes;
    Long session1;
    Long session2;
    Long session3;
    String inventoryString;
    String armorString;
    double health;
    Location location;
    Long lasJoin;
    String ip;




    public SQLPlayer(UUID uuid){
        this.uuid = uuid;
        if(!existsUUID()){
            throw new IllegalArgumentException("Spieler mit der UUID '" + uuid.toString() + "' ist nicht in der MySQL eingetragen!");
        }else {
            setName();
            setup();
        }
    }

    public SQLPlayer(String name){
        this.name = name;
        if(!existsName()){
            throw new IllegalArgumentException("Spieler mit dem Namen '" + name + "' ist nicht in der MySQL eingetragen!");
        }else {
            setUUID();
            setup();
        }
    }

    public SQLPlayer(String name, UUID uuid){
        this.name = name;
        this.uuid = uuid;
        if(!existsUUID() && !equals(name)) {
            insert(name, uuid);
        }
        setup();
    }

    public void setup(){
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM players WHERE uuid=?");
            st.setString(1, uuid.toString());
            ResultSet rs = st.executeQuery();
            rs.next();
            String[] kills = rs.getString("kills").split(",");
            this.kills = new ArrayList<UUID>();
            if(kills.length > 1) {
                for (String s : kills) {
                    this.kills.add(UUID.fromString(s));
                }
            }
            session1 = rs.getLong("session1");
            session2 = rs.getLong("session2");
            session3 = rs.getLong("session3");
            inventoryString = rs.getString("inventory");
            armorString = rs.getString("armor");
            health = rs.getDouble("health");
            double x = rs.getDouble("Xloc");
            double y = rs.getDouble("Yloc");
            double z = rs.getDouble("Zloc");
            String world = rs.getString("worldloc");
            if(Bukkit.getWorld(world) == null){
                location = null;
            }else {
                location = new Location(Bukkit.getWorld(world), x, y, z);
            }
            ip = rs.getString("ip");
            lasJoin = rs.getLong("lastjoin");

            String[] strikes = rs.getString("strikes").split(",");
            this.strikes = new ArrayList<String>();
            if(strikes.length == 1){
                this.strikes.add(rs.getString("strikes"));
            }else {
                Collections.addAll(this.strikes, strikes);
            }
            this.strikes.remove("");
            this.strikes.remove(" ");
            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void insert(String name, UUID uuid){
        try {
            PreparedStatement st = MySQL.c.prepareStatement("INSERT INTO players values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            st.setString(1, uuid.toString());
            st.setString(2, name);
            st.setString(3, "");
            st.setLong(4, 0);
            st.setLong(5, 0);
            st.setLong(6, 0);
            st.setString(7, "");
            st.setString(8, "");
            st.setDouble(9, 20);
            st.setDouble(10, 0);
            st.setDouble(11, 0);
            st.setDouble(12, 0);
            st.setString(13, "");
            st.setString(14, "");
            st.setString(15, "");
            st.setLong(16, System.currentTimeMillis());

            st.execute();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    public boolean existsUUID(){
        List<String> list = new ArrayList<>();
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM players WHERE uuid=?");
            st.setString(1, uuid.toString());

            ResultSet rs = st.executeQuery();
            while (rs.next()){
                list.add(rs.getString("uuid"));
            }
            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

       return list.contains(uuid.toString());
    }

    public boolean existsName(){
        List<String> list = new ArrayList<>();
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM players WHERE name=?");
            st.setString(1, name);

            ResultSet rs = st.executeQuery();
            while (rs.next()){
                list.add(rs.getString("name"));
            }
            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list.contains(name);
    }

    private void setName(){
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM players WHERE uuid=?");
            st.setString(1, uuid.toString());
            ResultSet rs = st.executeQuery();
            rs.next();
            name = rs.getString("name");
            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void setUUID(){
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM players WHERE name=?");
            st.setString(1, name);
            ResultSet rs = st.executeQuery();
            rs.next();
            uuid = UUID.fromString(rs.getString("uuid"));
            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public List<UUID> getKills() {
        return kills;
    }

    public Long getSession1() {
        return session1;
    }

    public Long getSession2() {
        return session2;
    }

    public Long getSession3() {
        return session3;
    }

    public String getInventoryString() {
        return inventoryString;
    }

    public double getHealth() {
        return health;
    }

    public Location getLocation() {
        if(Bukkit.getPlayer(getUuid()) == null) {
            updateLocation(Bukkit.getPlayer(getUuid()).getLocation());
    }
        return location;
    }

    public String getArmorString() {
        return armorString;
    }

    public List<String> getStrikes() {
        return strikes;
    }

    public void updateName(String s){
        MySQL.update("UPDATE players SET name='" + s + "' WHERE uuid='" + getUuid() + "';");
    }
    public void addKill(UUID uuid){
        String finalString = "";
        for (UUID uuid1 : getKills()){
            OfflineVaroPlayer player = new OfflineVaroPlayer(uuid1);
            finalString += player.getName() + ",";
        }

        OfflineVaroPlayer player = new OfflineVaroPlayer(uuid);
        finalString += player.getName();

        MySQL.update("UPDATE players SET kills='" + finalString + "' WHERE uuid='" + getUuid() + "';");
    }

    public void updateSession1(Long l){
        MySQL.update("UPDATE players SET session1=" + l + " WHERE uuid='" + getUuid() + "';");
    }

    public void updateSession2(Long l){
        MySQL.update("UPDATE players SET session2=" + l + " WHERE uuid='" + getUuid() + "';");
    }

    public void updateSession3(Long l){
        MySQL.update("UPDATE players SET session3=" + l + " WHERE uuid='" + getUuid() + "';");
    }

    public void updateInventory(String s){
        MySQL.update("UPDATE players SET inventory='" + s + "' WHERE uuid='" + getUuid() + "';");
    }

    public void updateArmor(String s){
        MySQL.update("UPDATE players SET armor='" + s + "' WHERE uuid='" + getUuid() + "';");
    }

    public void updateHealth(Double d){
        MySQL.update("UPDATE players SET health=" + d + " WHERE uuid='" + getUuid() + "';");
    }

    public void updateIP(String s){
        MySQL.update("UPDATE players SET ip='" + s + "' WHERE uuid='" + getUuid() + "';");
    }

    public void updatelastJoin(Long l){
        MySQL.update("UPDATE players SET lastjoin=" + l + " WHERE uuid='" + getUuid() + "';");
    }

    public void updateLocation(Location location){
        MySQL.update("UPDATE players SET Xloc=" + location.getX() + " WHERE uuid='" + getUuid() + "';");
        MySQL.update("UPDATE players SET Yloc=" + location.getY() + " WHERE uuid='" + getUuid() + "';");
        MySQL.update("UPDATE players SET Zloc=" + location.getZ() + " WHERE uuid='" + getUuid() + "';");
        MySQL.update("UPDATE players SET worldloc='" + location.getWorld().getName() + "' WHERE uuid='" + getUuid() + "';");
    }

    public void addStrike(String s){
        String finalString = "";
        for (String string : getStrikes()){
            finalString += string + ",";
        }

        finalString += s;

        MySQL.update("UPDATE players SET strikes='" + finalString + "' WHERE uuid='" + getUuid() + "';");
    }

    public void setStrikes(List<String> list){
        String finalString = "";
        int i = 1;
        for (String string : list){
            if(i == list.size()){
                finalString += string;
            }else {
                finalString += string + ",";
            }
            i++;

        }

        MySQL.update("UPDATE players SET strikes='" + finalString + "' WHERE uuid='" + getUuid() + "';");
    }

    public Long getLasJoin() {
        return lasJoin;
    }

    public String getIp() {
        return ip;
    }
}
