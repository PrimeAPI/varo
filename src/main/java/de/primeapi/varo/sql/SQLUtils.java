package de.primeapi.varo.sql;

import com.avaje.ebeaninternal.server.type.ScalarTypeEnumStandard;
import de.primeapi.varo.utils.OfflineVaroPlayer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SQLUtils {

    public static List<OfflineVaroPlayer> getAllPlayers(){
        List<OfflineVaroPlayer> list = new ArrayList<>();

        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM players");
            ResultSet rs = st.executeQuery();

            while (rs.next()){
                OfflineVaroPlayer curr = new OfflineVaroPlayer(UUID.fromString(rs.getString("uuid")));
                if(curr.getTeam() != null) {
                    list.add(curr);
                }
            }
            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static List<OfflineVaroPlayer> getAlivePlayers(){
        List<OfflineVaroPlayer> list = new ArrayList<>();
        for(OfflineVaroPlayer p : getAllPlayers()){
            if(p.getHealth() > 0 && p.getTeam() != null){
                list.add(p);
            }
        }
        return list;
    }

    public static List<UUID> getAliveUUIDS(){
        List<UUID> list = new ArrayList<>();
        for(OfflineVaroPlayer p : getAllPlayers()){
            if(p.getHealth() > 0 && p.getTeam() != null){
                list.add(p.getUuid());
            }
        }
        return list;
    }

    public static UUID getUUIDByIP(String ip){
        UUID uuid = null;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM players WHERE ip=?");
            st.setString(1, ip);
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                uuid = UUID.fromString(rs.getString("uuid"));
            }
            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

        return uuid;
    }

}
