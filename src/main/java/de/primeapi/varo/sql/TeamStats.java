package de.primeapi.varo.sql;

import com.sun.corba.se.impl.ior.FreezableList;
import de.primeapi.varo.utils.OfflineVaroPlayer;
import net.minecraft.server.v1_8_R3.EnumItemRarity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TeamStats {

    public static String getTeam(UUID uuid){
        if(getTeamByUUID1(uuid) != null){
            return getTeamByUUID1(uuid);
        }else if (getTeamByUUID2(uuid) != null){
            return getTeamByUUID2(uuid);
        }else {
            return null;
        }
    }

    private static String getTeamByUUID1(UUID uuid){
        String s = null;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM teams WHERE uuid1=?");
            st.setString(1, uuid.toString());
            ResultSet rs = st.executeQuery();

            if(rs.next()){
                s = rs.getString("name");
            }
            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return s;
    }

    private static String getTeamByUUID2(UUID uuid){
        String s = null;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM teams WHERE uuid2=?");
            st.setString(1, uuid.toString());
            ResultSet rs = st.executeQuery();

            if(rs.next()){
                s = rs.getString("name");
            }
            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return s;
    }

    public static UUID getMate(UUID uuid){
        if(getMateFrom1(uuid) != null){
            return getMateFrom1(uuid);
        }else if (getMateFrom2(uuid) != null){
            return getMateFrom2(uuid);
        }else {
            return null;
        }
    }

    public static void registerTeam(String name, OfflineVaroPlayer p1, OfflineVaroPlayer p2){
        try {
            PreparedStatement st = MySQL.c.prepareStatement("INSERT INTO teams values(?,?,?)");
            st.setString(1, name);
            st.setString(2, p1.getUuid().toString());
            st.setString(3, p2.getUuid().toString());

            st.execute();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<String> getAllTeams(){
        List<String> list = new ArrayList<>();
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM teams");
            ResultSet rs = st.executeQuery();

            while (rs.next()){
                list.add(rs.getString("name"));
            }

            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }


    private static UUID getMateFrom1(UUID uuid){
        String s = null;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM teams WHERE uuid1=?");
            st.setString(1, uuid.toString());
            ResultSet rs = st.executeQuery();

            if(rs.next()){
                s = rs.getString("uuid2");
            }
            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try{
            return UUID.fromString(s);
        }catch (IllegalArgumentException ex){
            return null;
        }
    }

    public static List<UUID> getUUIDByTeam(String team){
        List<UUID> list = new ArrayList<>();
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM teams WHERE name=?");
            st.setString(1, team);
            ResultSet rs = st.executeQuery();

            if(rs.next()){
                list.add(UUID.fromString(rs.getString("uuid1")));
                list.add(UUID.fromString(rs.getString("uuid2")));
            }
            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    private static UUID getMateFrom2(UUID uuid){
        String s = null;
        try {
            PreparedStatement st = MySQL.c.prepareStatement("SELECT * FROM teams WHERE uuid2=?");
            st.setString(1, uuid.toString());
            ResultSet rs = st.executeQuery();

            if(rs.next()){
                s = rs.getString("uuid1");
            }
            rs.close();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try{
            return UUID.fromString(s);
        }catch (IllegalArgumentException ex){
            return null;
        }
    }

}
