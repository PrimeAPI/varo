package de.primeapi.varo.sql;



import de.primeapi.varo.config.Conf;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQL {

    public static Connection c;

    public static void connect() {
        try {
            c = DriverManager.getConnection("jdbc:mysql://" + Conf.cfg.getString("mysql.host") + "/" + Conf.cfg.getString("mysql.database") + "?autoReconnect=true",
                    Conf.cfg.getString("mysql.username"),Conf.cfg.getString("mysql.password"));

            System.out.println("[Varo] MySQL Connected.");
            initMySQL();
        } catch (SQLException e) {
            System.out.println("[Varo] MySQL-Connection failed: " + e.getMessage());
        }
    }

    public static void disconnect() {
        if (c != null) {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void update(String qry) {
        try {
            Statement stmt = c.createStatement();
            stmt.executeUpdate(qry);
            stmt.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            disconnect();
            connect();
        }
    }

    private static void initMySQL(){
        //players: uuid, name, kills, session1, session2, session3, inventory, enderchest, health, Xloc, Yloc, Zloc, WorldLog
        //teams: name, uuid1, uuid2

        update("CREATE TABLE IF NOT EXISTS players(uuid varchar(1000),name varchar(1000),kills varchar(1000),session1 bigint,session2 bigint, session3 bigint," +
                " inventory text,armor text, health double, Xloc double, Yloc double, Zloc double,worldloc varchar(1000), strikes varchar(1000), ip varchar(1000), lastjoin bigint)");
        update("CREATE TABLE IF NOT EXISTS teams(name varchar(1000),uuid1 varchar(1000),uuid2 varchar(1000))");
    }

}
