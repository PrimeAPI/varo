package de.primeapi.varo.commands;

import de.primeapi.varo.utils.VaroPlayer;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class PingCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(!(commandSender instanceof Player)) {
            return false;
        }
        Player player = (Player) commandSender;
        VaroPlayer p = new VaroPlayer(player);

        p.sM(" Dein Ping beträgt: §e" + getPing(player));


        return true;
    }


    public int getPing(Player p) {
        CraftPlayer pingcraft = (CraftPlayer)p;
        EntityPlayer pingentity = pingcraft.getHandle();
        return pingentity.ping;
    }
}
