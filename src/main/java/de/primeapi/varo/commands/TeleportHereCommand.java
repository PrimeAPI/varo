package de.primeapi.varo.commands;

import de.primeapi.varo.utils.OfflineVaroPlayer;
import de.primeapi.varo.utils.VaroPlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeleportHereCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(!(commandSender instanceof Player)) {
            return false;
        }
        Player player = (Player) commandSender;
        VaroPlayer p = new VaroPlayer(player);

        if(!p.hasPermission("varo.tp")){
            p.sendNoPerm();
            return true;
        }

        if(args.length != 1){
            p.sendError(" Benutze: §e/tphere <spieler>");
            return true;
        }

        if(Bukkit.getPlayer(args[0]) == null){
            OfflineVaroPlayer t;
            try {
                t = new OfflineVaroPlayer(args[0]);
            }catch (Exception ex){
                p.sendError(" Dieser Spieler ist nicht in der Datenbank registriert!");
                return true;
            }

            t.updateLocation(p.thePlayer().getLocation());
            p.sendSuccess(" Der Spieler §e" + t.getName() + "§7 wird sobald er online kommt zu deiner Position teleportiert!");
            return true;

        }else {
            VaroPlayer t = new VaroPlayer(Bukkit.getPlayer(args[0]));


            t.thePlayer().teleport(p.thePlayer().getLocation());
            p.sendSuccess(" Du hast §e" + t.getName() + "§7 zu deiner Position teleportiert!");
        }



        return true;
    }
}
