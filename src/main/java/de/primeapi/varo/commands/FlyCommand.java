package de.primeapi.varo.commands;

import de.primeapi.varo.utils.Data;
import de.primeapi.varo.utils.VaroPlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FlyCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(!(commandSender instanceof Player)) {
            return false;
        }
        Player player = (Player) commandSender;
        VaroPlayer p = new VaroPlayer(player);

        if(!p.hasPermission("varo.fly")){
            p.sendNoPerm();
            return true;
        }

        if(args.length == 0) {
            if (p.thePlayer().getAllowFlight()) {
                p.thePlayer().setAllowFlight(false);
                p.thePlayer().setFlying(false);
                p.sendSuccess(" Dein Flugmodus wurde §cdeaktiviert§7!");
            } else {
                p.thePlayer().setAllowFlight(true);
                p.sendSuccess(" Dein Flugmodus wurde §aaktiviert§7!");
            }
        }else {
            Player t = Bukkit.getPlayer(args[0]);
            if(t == null){
                p.sendError(" Dieser Spieler existiert nicht!");
                return true;
            }
            if (t.getAllowFlight()) {
                t.setAllowFlight(false);
                t.setFlying(false);
                t.sendMessage(Data.prSuccess + " Dein Flugmodus wurde §cdeaktiviert§7!");
                p.sendSuccess(" Du hast den Flugmodus von §e" + t.getName() + "§c deaktiviert§7!");
            } else {
                t.setAllowFlight(true);
                t.sendMessage(Data.prSuccess + " Dein Flugmodus wurde §aaktiviert§7!");
                p.sendSuccess(" Du hast den Flugmodus von §e" + t.getName() + "§a aktiviert§7!");
            }

        }


        return true;
    }
}
