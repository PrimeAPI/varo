package de.primeapi.varo.commands;

import de.primeapi.varo.utils.Data;
import de.primeapi.varo.utils.Utils;
import de.primeapi.varo.utils.VaroPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SessionCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player)) {
            return false;
        }
        Player player = (Player) commandSender;
        VaroPlayer p = new VaroPlayer(player);

        if(p.getTeam() == null){
            p.sendError(" Du bist nicht als Spieler Registriert!");
            return true;
        }

        if(Data.sessioning.containsKey(p.getUuid())){
            p.sM(" Deine session läuft noch §e" + Data.sessioning.get(p.getUuid()) + " Sekunden§7!");
            return true;
        }
        Utils.startSession(p);

        return true;
    }
}
