package de.primeapi.varo.commands;

import de.primeapi.varo.utils.Data;
import de.primeapi.varo.utils.VaroPlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HealCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(!(commandSender instanceof Player)) {
            return false;
        }
        Player player = (Player) commandSender;
        VaroPlayer p = new VaroPlayer(player);

        if(!p.hasPermission("varo.heal")){
            p.sendNoPerm();
            return true;
        }

        if(args.length == 0){
            p.thePlayer().setHealth(20);
            p.thePlayer().setFoodLevel(25);
            p.sendSuccess(" Du wurdest geheilt!");
            return true;
        }

        if(args[0].equalsIgnoreCase("all") || args[0].equalsIgnoreCase("*")){
            for(Player all : Bukkit.getOnlinePlayers()){
                all.setHealth(20);
                all.sendMessage(Data.prSuccess + " Du wurdest geheilt!");
                all.setFoodLevel(25);
            }
            p.sendSuccess(" Du hast alle Spieler geheilt!");
            return true;
        }

        Player target = Bukkit.getPlayer(args[0]);
        if(target == null){
            p.sendError(" Dieser Spieler existiert nicht!");
            return true;
        }

        target.setHealth(20);
        target.sendMessage(Data.prSuccess + " Du wurdest geheilt!");
        target.setFoodLevel(25);


        p.sendSuccess(" Du hast §e" + target.getName() + " geheilt!");


        return true;
    }
}