package de.primeapi.varo.commands;

import de.primeapi.varo.utils.*;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class ManageCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(!(commandSender instanceof Player)) {
            return false;
        }
        Player player = (Player) commandSender;
        VaroPlayer p = new VaroPlayer(player);

        if(!p.hasPermission("varo.manage")){
            p.sendNoPerm();
            return true;
        }

        if(args.length == 0){
            p.sendError(" Benutze: §e/manage <Spieler>");
            return true;
        }


        if(Bukkit.getPlayer(args[0]) == null){
            OfflineVaroPlayer t;
            try{
                t = new OfflineVaroPlayer(args[0]);
            }catch (Exception ex){
                p.sendError(" Dieser Spieler wurde nicht gefunden!");
                return true;
            }
            //manager PrimeAPI invsee
            if(args.length < 2){
                p.sendError(" Benutze: §e/manage <Spieler> invsee");
                //p.sendError(" Benutze: §e/manage <Spieler> viewarmor");
                p.sendError(" Benutze: §e/manage <Spieler> getheath");
                p.sendError(" Benutze: §e/manage <Spieler> setheath <amount>");
                p.sendError(" Benutze: §e/manage <Spieler> info");
                p.sendError(" Benutze: §e/manage <Spieler> kill");
                p.sendError(" Benutze: §e/manage <Spieler> revive");
                p.sendError(" Benutze: §e/manage <Spieler> resetsessions");
                return true;
            }

            if(args[1].equalsIgnoreCase("invsee")){
                Inventory inv = new InventoryBuilder().setHolder(null).setSize(36).setName("§e§lInventar von " + t.getName()).build();
                inv.setContents(t.getInventory());
                p.thePlayer().openInventory(inv);
                Data.invsee.put(p.getUuid(), t.getUuid());
                return true;
            }

            if(args[1].equalsIgnoreCase("gethealth")){
                p.sM(" Der Spieler §e" + t.getName() + "§7 hat §e" + t.getHealth() + "§7 Leben!");
                return true;
            }

            if(args[1].equalsIgnoreCase("info")){
                p.sM(" Informationen über §e" + t.getName());
                p.sM(" Kills: §e" + t.getKills().size());
                p.sM(" Letzte Session:: §e" + Utils.getDate(t.getLasJoin()));
                p.sM(" Team: §e" + t.getTeam());
                p.sM(" Leben: §e" + t.getHealth());
                p.sM(" Team-Mate: §e" + t.getMate().getName());
                p.sM(" Position: §e" + t.getLocation().getBlockX() + "," + t.getLocation().getBlockY() + "," + t.getLocation().getBlockZ());
                return true;
            }

            if(args[1].equalsIgnoreCase("sethealth")){
                if(args.length != 3){
                    p.sendError(" Benutze: §e/manage <Spieler> setheath <amount>");
                    return true;
                }
                int i = 0;
                try {
                    i = Integer.valueOf(args[2]);
                }catch (Exception ex){
                    p.sendError(" Die angegebene Zahl ist ungülltig");
                    return true;
                }

                t.updateHealth((double) i);
                p.sendSuccess(" Du hast das Leben von §e" + t.getName() + "§7 auf §e" + i + "§7 gesetzt!");
                return true;
            }

            if(args[1].equalsIgnoreCase("kill")){
                t.updateHealth(0D);
                p.sM(" Du hast den Spieler §e" + t.getName() + "§7 von dem Spiel ausgeschlossen!");
                return true;
            }
            if(args[1].equalsIgnoreCase("revive")){
                t.updateHealth(20D);
                p.sM(" Du hast den Spieler §e" + t.getName() + "§7 wiederbelebt!");
                return true;
            }
            if(args[1].equalsIgnoreCase("resetsessions")){
                t.updateSession1(System.currentTimeMillis());
                t.updateSession2(System.currentTimeMillis());
                t.updateSession3(System.currentTimeMillis());
                p.sM(" Du hast den Spieler §e" + t.getName() + "§7 entsperrt!");
                return true;
            }

        }else{
            VaroPlayer t = new VaroPlayer(Bukkit.getPlayer(args[0]));
            //manager PrimeAPI invsee
            if(args.length < 2){
                p.sendError(" Benutze: §e/manage <Spieler> invsee");
                //p.sendError(" Benutze: §e/manage <Spieler> viewarmor");
                p.sendError(" Benutze: §e/manage <Spieler> getheath");
                p.sendError(" Benutze: §e/manage <Spieler> setheath <amount>");
                p.sendError(" Benutze: §e/manage <Spieler> info");
                p.sendError(" Benutze: §e/manage <Spieler> kill");
                p.sendError(" Benutze: §e/manage <Spieler> revive");
                p.sendError(" Benutze: §e/manage <Spieler> resetsessions");
                return true;
            }

            if(args[1].equalsIgnoreCase("invsee")){
                p.thePlayer().openInventory(t.thePlayer().getInventory());
                return true;
            }

            if(args[1].equalsIgnoreCase("gethealth")){
                p.sM(" Der Spieler §e" + t.getName() + "§7 hat §e" + t.thePlayer().getHealth() + "§7 Leben!");
                return true;
            }

            if(args[1].equalsIgnoreCase("info")){
                p.sM(" Informationen über §e" + t.getName());
                p.sM(" Kills: §e" + t.getKills().size());
                p.sM(" Letzte Session:: §e" + Utils.getDate(t.getLasJoin()));
                p.sM(" Team: §e" + t.getTeam());
                p.sM(" Leben: §e" + t.thePlayer().getHealth());
                p.sM(" Team-Mate: §e" + t.getMate().getName());
                p.sM(" Position: §e" + t.thePlayer().getLocation().getBlockX() + "," + t.thePlayer().getLocation().getBlockY() + "," + t.thePlayer().getLocation().getBlockZ());
                return true;
            }


            if(args[1].equalsIgnoreCase("sethealth")){
                if(args.length != 3){
                    p.sendError(" Benutze: §e/manage <Spieler> setheath <amount>");
                    return true;
                }
                int i = 0;
                try {
                    i = Integer.valueOf(args[2]);
                }catch (Exception ex){
                    p.sendError(" Die angegebene Zahl ist ungülltig");
                    return true;
                }

                t.setHealth((double) i);
                p.sendSuccess(" Du hast das Leben von §e" + t.getName() + "§7 auf §e" + i + "§7 gesetzt!");
                return true;
            }

            if(args[1].equalsIgnoreCase("kill")){
                t.thePlayer().setHealth(0D);
                t.updateHealth(0D);
                p.sM(" Du hast den Spieler §e" + t.getName() + "§7 von dem Spiel ausgeschlossen!");
                return true;
            }
            if(args[1].equalsIgnoreCase("revive")){
                t.updateHealth(20D);
                p.sM(" Du hast den Spieler §e" + t.getName() + "§7 wiederbelebt!");
                return true;
            }

            if(args[1].equalsIgnoreCase("resetsessions")){
                t.updateSession1(System.currentTimeMillis());
                t.updateSession2(System.currentTimeMillis());
                t.updateSession3(System.currentTimeMillis());
                p.sM(" Du hast den Spieler §e" + t.getName() + "§7 entsperrt!");
                return true;
            }

        }



        return true;
    }
}
