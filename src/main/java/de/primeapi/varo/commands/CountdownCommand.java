package de.primeapi.varo.commands;

import de.primeapi.varo.Varo;
import de.primeapi.varo.config.Conf;
import de.primeapi.varo.utils.Data;
import de.primeapi.varo.utils.Scoreboard;
import de.primeapi.varo.utils.Utils;
import de.primeapi.varo.utils.VaroPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Score;

import java.util.UUID;

public class CountdownCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player)) {
            return false;
        }
        Player player = (Player) commandSender;
        VaroPlayer p = new VaroPlayer(player);

        if(!p.hasPermission("varo.admin")){
            p.sendNoPerm();
            return true;
        }

        if(Conf.cfg.getBoolean("settings.started")){
            p.sendError(" Varo ist bereits gestartet!");
            return true;
        }

        if(Data.cd != 30){
            p.sendError(" Der Countdown wurde bereits gestartet!");
            return true;
        }

        int i = Bukkit.getScheduler().scheduleSyncRepeatingTask(Varo.getInstance(), () -> {
            if(Data.cd == 0){
                Data.cd--;
                Conf.cfg.set("settings.started", true);
                Conf.save();
                Data.started = true;
                for(Player all : Bukkit.getOnlinePlayers()){
                    VaroPlayer vp = new VaroPlayer(all);
                    if(vp.getTeam() != null){
                        Utils.startSession(vp, true);
                        all.setMaxHealth(20);
                        all.setHealth(20);
                        all.setFoodLevel(25);
                        all.setGameMode(GameMode.SURVIVAL);
                    }
                }
                Data.frozen.clear();
                Bukkit.broadcastMessage(Data.pr + " Varo ist gestartet!");
                Data.cd = 30;
                for(World world : Bukkit.getWorlds()){
                    world.setDifficulty(Difficulty.NORMAL);
                }
                Scoreboard.updateScore();
            }else if (Data.cd > 0 && !Data.started){
                Bukkit.broadcastMessage(Data.pr + " Varo startet in §e" + Data.cd + " Sekunden§7!");
                Data.cd--;
                Scoreboard.updateScore();
            }
        }, 20, 20);






        return true;
    }
}
