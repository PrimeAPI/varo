package de.primeapi.varo.commands;

import de.primeapi.varo.managers.LocationManager;
import de.primeapi.varo.utils.VaroPlayer;
import javafx.print.PageLayout;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetLocationCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(!(commandSender instanceof Player)) {
            return false;
        }
        Player player = (Player) commandSender;
        VaroPlayer p = new VaroPlayer(player);

        if(!p.hasPermission("varo.admin")){
            p.sendNoPerm();
            return true;
        }

        if(args.length == 0){
            p.sendError(" §7Benutze: §e/" + command.getName() + " §e<pos>");
            return true;
        }

        LocationManager.setLocation(args[0], player.getLocation());
        p.sendSuccess(" Die Location §8\"§e" + args[0] + "§8\"§7 wurde gesetzt!");


        return true;
    }
}
