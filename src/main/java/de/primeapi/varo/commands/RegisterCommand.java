package de.primeapi.varo.commands;

import de.primeapi.varo.sql.SQLPlayer;
import de.primeapi.varo.sql.TeamStats;
import de.primeapi.varo.utils.OfflineVaroPlayer;
import de.primeapi.varo.utils.VaroPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class RegisterCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(!(commandSender instanceof Player)) {
            return false;
        }
        Player player = (Player) commandSender;
        VaroPlayer p = new VaroPlayer(player);

        if(!p.hasPermission("varo.admin")){
            p.sendNoPerm();
            return true;
        }


        //register team <name> <player1> <player2>
        //register player <name> <uuid>
        if(args.length <= 2){
            p.sendError(" Benutze: §e/register team <Name> <Spieler1> <Spieler2>");
            p.sendError(" Benutze: §e/register player <Name> <UUID>");
            return true;
        }

        if(args[0].equalsIgnoreCase("team")){

            if(args.length != 4){
                p.sendError(" Benutze: §e/register team <Name> <Spieler1> <Spieler2>");
                return true;
            }

            OfflineVaroPlayer p1;
            OfflineVaroPlayer p2;
            try {
                p1 = new OfflineVaroPlayer(args[2]);
            }catch (Exception ex){
                p.sendError(" Der Spieler §8\"§e" + args[2] + "§8\" §7existiert nicht!");
                return true;
            }

            try {
                p2 = new OfflineVaroPlayer(args[3]);
            }catch (Exception ex){
                p.sendError(" Der Spieler §8\"§e" + args[3] + "§8\" §7existiert nicht!");
                return true;
            }

            if(TeamStats.getUUIDByTeam(args[1]).size() > 0){
                p.sendError(" Das Team existiert bereits!");
                return true;
            }

            TeamStats.registerTeam(args[1], p1, p2);
            p.sendSuccess("§7 Du hast das Team §8\"§e" + args[1] + "§8\" §7mit den Teilnehmern §e" + p1.getName() + "§7 und §e" + p2.getName() + "§a erstellt§7!");

            return true;
        }

        if(args[0].equalsIgnoreCase("player")){
            String name = args[1];
            UUID uuid;
            try {
                uuid = UUID.fromString(args[2]);
            }catch (Exception ex){
                p.sendError(" Die angegebene UUID ist ungültig!");
                return true;
            }

            boolean exists = false;

            try {
                SQLPlayer sqlPlayer = new SQLPlayer(name);
                exists = true;
            }catch (Exception ex){
                exists = false;
            }

            try {
                SQLPlayer sqlPlayer = new SQLPlayer(uuid);
                exists = true;
            }catch (Exception ex){
                exists = false;
            }

            if(exists){
                p.sendError("§7 Dieses Spieler ist bereits eingetragen!");
                return true;
            }

            new SQLPlayer(name, uuid);
            p.sendSuccess("§7 Du hast den Spieler §e" + name + "§7 mit der UUID §e" + uuid.toString() + "§7 in die Datenbank eingetragen!");
            return true;
        }

        p.sendError(" Benutze: §e/register team <name> <Spieler1> <Spieler2>");
        p.sendError(" Benutze: §e/register player <Name> <UUID>");

        return true;
    }
}
