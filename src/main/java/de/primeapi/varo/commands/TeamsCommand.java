package de.primeapi.varo.commands;

import de.primeapi.varo.sql.SQLUtils;
import de.primeapi.varo.sql.TeamStats;
import de.primeapi.varo.utils.Data;
import de.primeapi.varo.utils.OfflineVaroPlayer;
import de.primeapi.varo.utils.VaroPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

public class TeamsCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player)) {
            return false;
        }
        Player player = (Player) commandSender;
        VaroPlayer p = new VaroPlayer(player);

        p.sM(" Alle registrierten Team:");
        for(String name : TeamStats.getAllTeams()){
            List<UUID> list = TeamStats.getUUIDByTeam(name);
            OfflineVaroPlayer p1 = new OfflineVaroPlayer(list.get(0));
            OfflineVaroPlayer p2 = new OfflineVaroPlayer(list.get(1));

            p.sM("§b " + name + "§8: " + (canPlay(p1) ? "§a" : "§c") + p1.getName() + "§7, " + (canPlay(p2) ? "§a" : "§c") + p2.getName() );

        }


        return true;
    }

    public boolean canPlay(OfflineVaroPlayer p){
        if((int) p.getHealth() == 0){
            return false;
        }

        if(p.getStrikes().size() >= Data.maxStrikes){
            return false;
        }

        if(p.getTeam() == null){
            return false;
        }
        return true;
    }

}
