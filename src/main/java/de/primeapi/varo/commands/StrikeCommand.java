package de.primeapi.varo.commands;

import de.primeapi.varo.utils.Data;
import de.primeapi.varo.utils.OfflineVaroPlayer;
import de.primeapi.varo.utils.VaroPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class StrikeCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String lable, String[] args) {
        if (!(commandSender instanceof Player)) {
            return false;
        }
        Player player = (Player) commandSender;
        VaroPlayer p = new VaroPlayer(player);

        if (!p.hasPermission("varo.strike")) {
            p.sendNoPerm();
            return true;
        }

        if(args.length == 0){
            p.sendError(" §7Benutze: §e/Strike <Spieler>");
            return true;
        }

        OfflineVaroPlayer t;
        try {
            t = new OfflineVaroPlayer(args[0]);
        }catch (Exception ex) {
            p.sendError(" §7Dieser Spieler existiert nicht!");
            return true;
        }

        if(args.length == 1){
            p.sendMessage(" Alle Strikes von §e" + t.getName());
            p.sendMessage(" ");
            int i = 1;
            for(String s : t.getStrikes()){
                p.sendMessage(" #" + i + " - §e" + s);
                i++;
            }
            p.sendMessage(" ");
            p.sendMessage(" Lösche einen Strike mit /strike " + t.getName() + " remove <Strike-ID>");
            return true;
        }



        if(args[1].equalsIgnoreCase("remove")){
            int row;
            try {
                row = Integer.valueOf(args[2]);
            }catch (Exception ex){
                p.sendError("§7 Bitte gib eine Zahl an!");
                return true;
            }
            List<String> list = t.getStrikes();

            int i = 1;
            boolean b = false;
            String rem = "";
            for(String s : list){
                if(i == row && !b){
                    rem = s;
                    b = true;
                }else if(!b){
                    i++;
                }
            }
            
            list.remove(rem);
            if(!b){
                p.sendError("§7 Der Spieler hat diesen Strike nicht!");
                return true;
            }

            t.setStrikes(list);
            p.sendSuccess("§7 Du hast den §e" + row + ". Strike §7von §e" + t.getName() + "§7 entfernt!");
            return true;
        }else if(args[1].equalsIgnoreCase("add")){
            if(args.length >= 3){
                t.addStrike(args[2]);
                p.sendSuccess("§7 Du hast §e" + t.getName() + "§7 erfolgreich wegen §e" + args[2] + "§7 gestriked!");

                int i = t.getStrikes().size() + 1;
                if(i >= Data.cordLeak){
                    p.sendMessage(" §cACHTUNG: §7Dieser Spieler hat nun §e" + i + "§7 strikes!");
                    Location location = t.getLocation();
                    p.sendMessage(" Veröffentliche nun seine Coordinaten: §e" + location.getBlockX() + "§7, §e" + location.getBlockY() + "§7, §e" + location.getBlockZ());
                }

                if(i >= Data.invClear){
                    p.sendMessage(" §cACHTUNG: §7Das Inventar des Spielers wurde geleert!");
                    t.updateInventory(Bukkit.createInventory(null, 36));
                }

                if(i >= Data.maxStrikes){
                    p.sendMessage(" §cACHTUNG: §7Der Spieler wurde von Projekt ausgeschlossen!");
                }


                return true;
            }
            return true;
        }else {
            p.sendMessage(" Alle Strikes von §e" + t.getName());
            p.sendMessage(" ");
            int i = 1;
            for(String s : t.getStrikes()){
                p.sendMessage(" #" + i + " - §e" + s);
                i++;
            }
            p.sendMessage(" ");
            p.sendMessage(" Lösche einen Strike mit /strike " + t.getName() + " remove <Strike-ID>");
            return true;
        }

    }

}
