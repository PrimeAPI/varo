package de.primeapi.varo.commands;

import de.primeapi.varo.utils.VaroPlayer;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GamemodeCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if(!(commandSender instanceof Player)) {
            return false;
        }
        Player player = (Player) commandSender;
        VaroPlayer p = new VaroPlayer(player);

        if(!p.hasPermission("varo.gamemode")){
            p.sendNoPerm();
            return true;
        }

        if(args.length == 0){
            p.sendError(" Benutze: §e/" + command + " <0,1,2,3>");
            return true;
        }

        if(args.length == 1){
            GameMode mode;
            if(args[0].equalsIgnoreCase("0")){
                mode = GameMode.SURVIVAL;
            }else if(args[0].equalsIgnoreCase("1")){
                mode = GameMode.CREATIVE;
            }else if(args[0].equalsIgnoreCase("2")){
                mode = GameMode.ADVENTURE;
            }else if(args[0].equalsIgnoreCase("3")){
                mode = GameMode.SPECTATOR;
            }else {
                p.sendError(" Benutze: §e/" + command.getName() + " <0,1,2,3>");
                return true;
            }

            p.thePlayer().setGameMode(mode);
            p.sendSuccess(" Dein Spielmodus wurde zu §e" + mode.toString() + "§7 geändert!");
            return true;
        }else {
            Player tp = Bukkit.getPlayer(args[1]);
            if(tp == null){
                p.sendError(" Der angegebene Spieler ist nicht online!");
                return true;
            }
            VaroPlayer t = new VaroPlayer(tp);
            GameMode mode;
            if(args[0].equalsIgnoreCase("0")){
                mode = GameMode.SURVIVAL;
            }else if(args[0].equalsIgnoreCase("1")){
                mode = GameMode.CREATIVE;
            }else if(args[0].equalsIgnoreCase("2")){
                mode = GameMode.ADVENTURE;
            }else if(args[0].equalsIgnoreCase("3")){
                mode = GameMode.SPECTATOR;
            }else {
                p.sendError(" Benutze: §e/" + command + " <0,1,2,3>");
                return true;
            }

            t.thePlayer().setGameMode(mode);
            t.sendSuccess(" Dein Spielmodus wurde zu §e" + mode.toString() + "§7 geändert!");
            p.sendSuccess(" Du hast den Spielmodus von §e" + t.getName() + " zu §e" + mode.toString() + "§7 geändert!");

        }

        return true;
    }
}
