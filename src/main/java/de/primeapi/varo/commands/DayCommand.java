package de.primeapi.varo.commands;

import de.primeapi.varo.utils.VaroPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DayCommand  implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player)) {
            return false;
        }
        Player player = (Player) commandSender;
        VaroPlayer p = new VaroPlayer(player);

        if(!p.hasPermission("varo.day")){
            p.sendNoPerm();
            return true;
        }

        p.thePlayer().getLocation().getWorld().setTime(0L);
        p.sendSuccess(" Du hast die Zeit auf §e0§7 gesetzt!");

        return true;
    }

}
