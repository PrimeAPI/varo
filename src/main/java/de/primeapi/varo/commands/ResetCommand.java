package de.primeapi.varo.commands;

import de.primeapi.varo.config.Conf;
import de.primeapi.varo.sql.MySQL;
import de.primeapi.varo.utils.Data;
import de.primeapi.varo.utils.VaroPlayer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class ResetCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            return false;
        }
        Player player = (Player) commandSender;
        VaroPlayer p = new VaroPlayer(player);

        if (!p.hasPermission("varo.reset")) {
            p.sendNoPerm();
            return true;
        }

        if(args.length == 0){
            p.sendMessage(" §7Bist du dir Sicher, dass du dieses Varo §c§nunwiederruflich löschen§7 willst?");
            TextComponent component = new TextComponent("§8[§cJa§8]");
            component.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/reset confirm"));
            component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cBeende dieses Varo §4§nunwiederruflich§c!").create()));
            p.thePlayer().spigot().sendMessage(new TextComponent(Data.pr + " "), component);
            return true;
        }

        if(args[0].equalsIgnoreCase("confirm")){
            Conf.cfg.set("settings.started", false);
            Conf.save();

            MySQL.update("DELETE FROM players");
            MySQL.update("DELETE FROM teams");

            Bukkit.reload();
        }else {
            p.sendMessage(" §7Bist du dir Sicher, dass du dieses Varo §c§nunwiederruflich löschen§7 willst?");
            TextComponent component = new TextComponent("§8[§cJa§8]");
            component.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/reset confirm"));
            component.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§cBeende dieses Varo §4§nunwiederruflich§c!").create()));
            p.thePlayer().spigot().sendMessage(component);
            return true;
        }



        return true;
    }
}