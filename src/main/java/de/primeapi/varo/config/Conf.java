package de.primeapi.varo.config;

import de.primeapi.varo.Varo;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class Conf {

    public static File file;
    public static YamlConfiguration cfg;

    public static void init(){
        Varo.getThreadPoolExecutor().submit(() -> {
            File ord = new File("plugins/varo");
            if(!ord.exists()) ord.mkdir();
        });
        file = new File("plugins/varo", "config.yml");

        boolean isnew = false;
        try {
            cfg = YamlConfiguration.loadConfiguration(file);
        }catch (Exception ex){
            isnew = true;
            Varo.getThreadPoolExecutor().submit(() -> {
                try {
                    file.createNewFile();
                    cfg = YamlConfiguration.loadConfiguration(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }

        if(!cfg.contains("mysql.host")) {
            Varo.getThreadPoolExecutor().submit(() -> {
                cfg.set("mysql.host", "127.0.0.1:3306");
                cfg.set("mysql.database", "varo");
                cfg.set("mysql.username", "root");
                cfg.set("mysql.password", "password");

                cfg.set("settings.started", false);

                cfg.set("settings.prefix.normal", "&1│ &bVaro &1»&7");
                cfg.set("settings.prefix.error", "&4│ &cVaro &4»&7");
                cfg.set("settings.prefix.success", "&2│ &aVaro &2»&7");

                cfg.set("settings.sponsor", "&ePrimeShop &8| &eprimeshopmc.de");
                cfg.set("settings.ip", "Varo.de");

                cfg.set("settings.session.lenghtInSecconds", 15 * 60);
                cfg.set("settings.session.wait", (long) (24 * 60 * 60 * 1000));
                cfg.set("settings.session.premiumsession.use", true);
                cfg.set("settings.session.premiumsession.lenghtInSecconds", 20 * 60);
                cfg.set("settings.session.premiumsession.lowerBoarder", 16);
                cfg.set("settings.session.premiumsession.upperBoarder", 18);

                cfg.set("settings.strikes.autoban", 5);
                cfg.set("settings.strikes.leakCoordinates", 2);
                cfg.set("settings.strikes.clearInventory", 4);

                cfg.set("settings.worldboarder.size", 2000);
                cfg.set("settings.worldboarder.reduce.active", true);
                cfg.set("settings.worldboarder.reduce.amount", 100);

                try {
                    cfg.save(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public static void save(){
        Varo.getThreadPoolExecutor().submit(() -> {
            try {
                cfg.save(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public static void reload(){
        try {
            cfg = YamlConfiguration.loadConfiguration(file);
        }catch (Exception ex){
            Varo.getThreadPoolExecutor().submit(() -> {
                try {
                    file.createNewFile();
                    cfg = YamlConfiguration.loadConfiguration(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public static String getString(String key){
        return cfg.getString(key).replaceAll("&", "§");
    }


}
