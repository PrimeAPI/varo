package de.primeapi.varo.utils;

import de.primeapi.varo.Varo;
import de.primeapi.varo.sql.SQLUtils;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.util.Random;

public class Scoreboard {

    public static void updateScore(){
        updateScore(false);
    }



    public static void updateScore(Boolean join){
        for(Player all : Bukkit.getOnlinePlayers()) {
            VaroPlayer p = new VaroPlayer(all);
            if(!Data.started){
                sendWait(p);
            }else if (Data.sessioning.containsKey(p.getUuid())) {
                sendIngame(p);
            }else if(p.hasPermission("varo.admin") && join){
                sendAdmin(p);
            }
        }
    }

    public static void sendIngame(VaroPlayer p){
        net.minecraft.server.v1_8_R3.Scoreboard scoreboard = new net.minecraft.server.v1_8_R3.Scoreboard();
        ScoreboardObjective obj = scoreboard.registerObjective("zagd", IScoreboardCriteria.b);
        obj.setDisplayName("§a§lVaro");

        PacketPlayOutScoreboardObjective createPacket = new PacketPlayOutScoreboardObjective(obj, 0);
        PacketPlayOutScoreboardDisplayObjective display = new PacketPlayOutScoreboardDisplayObjective(1, obj);

        ScoreboardScore s1 = new ScoreboardScore(scoreboard, obj, "§7§l            ");
        ScoreboardScore s2 = new ScoreboardScore(scoreboard, obj, "§7Verbleibende Zeit:");
        ScoreboardScore s3 = new ScoreboardScore(scoreboard, obj, "§8➥ §e" + p.getRemainingSession());
        ScoreboardScore s4 = new ScoreboardScore(scoreboard, obj, "§1§l            ");
        ScoreboardScore s5 = new ScoreboardScore(scoreboard, obj, "§7Kills:");
        ScoreboardScore s6 = new ScoreboardScore(scoreboard, obj, "§8➥ §e" + p.getKills().size());
        ScoreboardScore s7 = new ScoreboardScore(scoreboard, obj, "§2§l            ");
        ScoreboardScore s8 = new ScoreboardScore(scoreboard, obj, "§7Lebende Spieler:");
        ScoreboardScore s9 = new ScoreboardScore(scoreboard, obj, "§8➥ §e" + SQLUtils.getAlivePlayers().size());
        ScoreboardScore s10 = new ScoreboardScore(scoreboard, obj, "§3§l§" + new Random().nextInt(8) + "§" + new Random().nextInt(8) + "§" + new Random().nextInt(8) + "§" + new Random().nextInt(8));

        s1.setScore(10);
        s2.setScore(9);
        s3.setScore(8);
        s4.setScore(7);
        s5.setScore(6);
        s6.setScore(5);
        s7.setScore(4);
        s8.setScore(3);
        s9.setScore(2);
        s10.setScore(1);

        PacketPlayOutScoreboardObjective removePacket = new PacketPlayOutScoreboardObjective(obj, 1);
        PacketPlayOutScoreboardScore pa1 = new PacketPlayOutScoreboardScore(s1);
        PacketPlayOutScoreboardScore pa2 = new PacketPlayOutScoreboardScore(s2);
        PacketPlayOutScoreboardScore pa3 = new PacketPlayOutScoreboardScore(s3);
        PacketPlayOutScoreboardScore pa4 = new PacketPlayOutScoreboardScore(s4);
        PacketPlayOutScoreboardScore pa5 = new PacketPlayOutScoreboardScore(s5);
        PacketPlayOutScoreboardScore pa6 = new PacketPlayOutScoreboardScore(s6);
        PacketPlayOutScoreboardScore pa7 = new PacketPlayOutScoreboardScore(s7);
        PacketPlayOutScoreboardScore pa8 = new PacketPlayOutScoreboardScore(s8);
        PacketPlayOutScoreboardScore pa9 = new PacketPlayOutScoreboardScore(s9);
        PacketPlayOutScoreboardScore pa10 = new PacketPlayOutScoreboardScore(s10);

        sendPacket(removePacket, p.thePlayer());
        sendPacket(createPacket, p.thePlayer());
        sendPacket(display, p.thePlayer());

        sendPacket(pa1, p.thePlayer());
        sendPacket(pa2, p.thePlayer());
        sendPacket(pa3, p.thePlayer());
        sendPacket(pa4, p.thePlayer());
        sendPacket(pa5, p.thePlayer());
        sendPacket(pa6, p.thePlayer());
        sendPacket(pa7, p.thePlayer());
        sendPacket(pa8, p.thePlayer());
        sendPacket(pa9, p.thePlayer());
        sendPacket(pa10, p.thePlayer());



    }

    private static String getAble(VaroPlayer p){
        if(p.getTeam() == null){
            return "§cNicht Registriert";
        }
        if(p.getHealth() == 0){
            return "§cGestorben!";
        }
        if(Utils.getLowestSessionValue(p) > System.currentTimeMillis()){
            return Utils.getRemainingTimeShort(Utils.getLowestSessionValue(p));
        }
        if(p.getStrikes().size() >= Data.maxStrikes){
            return "§cGesperrt";
        }
        return "§aVerfügbar";
    }

    public static void sendAdmin(VaroPlayer p){
        net.minecraft.server.v1_8_R3.Scoreboard scoreboard = new net.minecraft.server.v1_8_R3.Scoreboard();
        ScoreboardObjective obj = scoreboard.registerObjective("zagd", IScoreboardCriteria.b);
        obj.setDisplayName("§a§lVaro §7✦ §cAdmin-Mode");

        PacketPlayOutScoreboardObjective createPacket = new PacketPlayOutScoreboardObjective(obj, 0);
        PacketPlayOutScoreboardDisplayObjective display = new PacketPlayOutScoreboardDisplayObjective(1, obj);

        ScoreboardScore s1 = new ScoreboardScore(scoreboard, obj, "§7§l            ");
        ScoreboardScore s2 = new ScoreboardScore(scoreboard, obj, "§7Verfügbare session:");
        ScoreboardScore s3 = new ScoreboardScore(scoreboard, obj, "§8➥ §e" + getAble(p));
        ScoreboardScore s4 = new ScoreboardScore(scoreboard, obj, "§1§l            ");
        ScoreboardScore s5 = new ScoreboardScore(scoreboard, obj, "§7Eingeloggte Spieler:");
        ScoreboardScore s6 = new ScoreboardScore(scoreboard, obj, "§8➥ §e" + Data.sessioning.size());
        ScoreboardScore s7 = new ScoreboardScore(scoreboard, obj, "§2§l            §" + new Random().nextInt(8));
        ScoreboardScore s8 = new ScoreboardScore(scoreboard, obj, "§7Lebende Spieler:");
        ScoreboardScore s9 = new ScoreboardScore(scoreboard, obj, "§8➥ §e" + SQLUtils.getAlivePlayers().size() + "§8/§c" + SQLUtils.getAllPlayers().size());
        ScoreboardScore s10 = new ScoreboardScore(scoreboard, obj, "§3§l§" + new Random().nextInt(8) + "§" + new Random().nextInt(8) + "§" + new Random().nextInt(8) + "§" + new Random().nextInt(8));

        s1.setScore(10);
        s2.setScore(9);
        s3.setScore(8);
        s4.setScore(7);
        s5.setScore(6);
        s6.setScore(5);
        s7.setScore(4);
        s8.setScore(3);
        s9.setScore(2);
        s10.setScore(1);

        PacketPlayOutScoreboardObjective removePacket = new PacketPlayOutScoreboardObjective(obj, 1);
        PacketPlayOutScoreboardScore pa1 = new PacketPlayOutScoreboardScore(s1);
        PacketPlayOutScoreboardScore pa2 = new PacketPlayOutScoreboardScore(s2);
        PacketPlayOutScoreboardScore pa3 = new PacketPlayOutScoreboardScore(s3);
        PacketPlayOutScoreboardScore pa4 = new PacketPlayOutScoreboardScore(s4);
        PacketPlayOutScoreboardScore pa5 = new PacketPlayOutScoreboardScore(s5);
        PacketPlayOutScoreboardScore pa6 = new PacketPlayOutScoreboardScore(s6);
        PacketPlayOutScoreboardScore pa7 = new PacketPlayOutScoreboardScore(s7);
        PacketPlayOutScoreboardScore pa8 = new PacketPlayOutScoreboardScore(s8);
        PacketPlayOutScoreboardScore pa9 = new PacketPlayOutScoreboardScore(s9);
        PacketPlayOutScoreboardScore pa10 = new PacketPlayOutScoreboardScore(s10);

        sendPacket(removePacket, p.thePlayer());
        sendPacket(createPacket, p.thePlayer());
        sendPacket(display, p.thePlayer());

        sendPacket(pa1, p.thePlayer());
        sendPacket(pa2, p.thePlayer());
        sendPacket(pa3, p.thePlayer());
        sendPacket(pa4, p.thePlayer());
        sendPacket(pa5, p.thePlayer());
        sendPacket(pa6, p.thePlayer());
        sendPacket(pa7, p.thePlayer());
        sendPacket(pa8, p.thePlayer());
        sendPacket(pa9, p.thePlayer());
        sendPacket(pa10, p.thePlayer());



    }

    public static void sendWait(VaroPlayer p){
        net.minecraft.server.v1_8_R3.Scoreboard scoreboard = new net.minecraft.server.v1_8_R3.Scoreboard();
        ScoreboardObjective obj = scoreboard.registerObjective("zagd", IScoreboardCriteria.b);
        obj.setDisplayName("§a§lVaro");

        PacketPlayOutScoreboardObjective createPacket = new PacketPlayOutScoreboardObjective(obj, 0);
        PacketPlayOutScoreboardDisplayObjective display = new PacketPlayOutScoreboardDisplayObjective(1, obj);

        ScoreboardScore s1 = new ScoreboardScore(scoreboard, obj, "§7§l            ");
        ScoreboardScore s2 = new ScoreboardScore(scoreboard, obj, "§7Countdown:");
        ScoreboardScore s3 = new ScoreboardScore(scoreboard, obj, "§8➥ §e" + (Data.cd == 30 ? "§cNicht gestartet" : Data.cd));
        ScoreboardScore s4 = new ScoreboardScore(scoreboard, obj, "§1§l            ");
        ScoreboardScore s5 = new ScoreboardScore(scoreboard, obj, "§7Registrierte Spieler:");
        ScoreboardScore s6 = new ScoreboardScore(scoreboard, obj, "§8➥ §e" + SQLUtils.getAlivePlayers().size());
        ScoreboardScore s7 = new ScoreboardScore(scoreboard, obj, "§2§l            ");
        ScoreboardScore s8 = new ScoreboardScore(scoreboard, obj, "§7Dein Team:");
        ScoreboardScore s9 = new ScoreboardScore(scoreboard, obj, "§8➥ §e" + p.getTeam());
        ScoreboardScore s10 = new ScoreboardScore(scoreboard, obj, "§3§l§" + new Random().nextInt(8) + "§" + new Random().nextInt(8) + "§" + new Random().nextInt(8) + "§" + new Random().nextInt(8));

        s1.setScore(10);
        s2.setScore(9);
        s3.setScore(8);
        s4.setScore(7);
        s5.setScore(6);
        s6.setScore(5);
        s7.setScore(4);
        s8.setScore(3);
        s9.setScore(2);
        s10.setScore(1);

        PacketPlayOutScoreboardObjective removePacket = new PacketPlayOutScoreboardObjective(obj, 1);
        PacketPlayOutScoreboardScore pa1 = new PacketPlayOutScoreboardScore(s1);
        PacketPlayOutScoreboardScore pa2 = new PacketPlayOutScoreboardScore(s2);
        PacketPlayOutScoreboardScore pa3 = new PacketPlayOutScoreboardScore(s3);
        PacketPlayOutScoreboardScore pa4 = new PacketPlayOutScoreboardScore(s4);
        PacketPlayOutScoreboardScore pa5 = new PacketPlayOutScoreboardScore(s5);
        PacketPlayOutScoreboardScore pa6 = new PacketPlayOutScoreboardScore(s6);
        PacketPlayOutScoreboardScore pa7 = new PacketPlayOutScoreboardScore(s7);
        PacketPlayOutScoreboardScore pa8 = new PacketPlayOutScoreboardScore(s8);
        PacketPlayOutScoreboardScore pa9 = new PacketPlayOutScoreboardScore(s9);
        PacketPlayOutScoreboardScore pa10 = new PacketPlayOutScoreboardScore(s10);

        sendPacket(removePacket, p.thePlayer());
        sendPacket(createPacket, p.thePlayer());
        sendPacket(display, p.thePlayer());

        sendPacket(pa1, p.thePlayer());
        sendPacket(pa2, p.thePlayer());
        sendPacket(pa3, p.thePlayer());
        sendPacket(pa4, p.thePlayer());
        sendPacket(pa5, p.thePlayer());
        sendPacket(pa6, p.thePlayer());
        sendPacket(pa7, p.thePlayer());
        sendPacket(pa8, p.thePlayer());
        sendPacket(pa9, p.thePlayer());
        sendPacket(pa10, p.thePlayer());



    }



    private static void sendPacket(@SuppressWarnings("rawtypes") Packet packet, Player p) {
        ((CraftPlayer)p).getHandle().playerConnection.sendPacket(packet);
    }
}
