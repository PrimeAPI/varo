package de.primeapi.varo.utils;

import com.sun.rmi.rmid.ExecPermission;
import de.primeapi.varo.sql.SQLPlayer;
import de.primeapi.varo.sql.TeamStats;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.io.IOException;
import java.util.UUID;

public class OfflineVaroPlayer extends SQLPlayer {
    public OfflineVaroPlayer(UUID uuid) {
        super(uuid);
    }

    public OfflineVaroPlayer(String name) {
        super(name);
    }

    public OfflineVaroPlayer(String name, UUID uuid) {
        super(name, uuid);
    }

    public ItemStack[] getInventory(){
        try {
            ItemStack[] items = InventorySerialization.itemStackArrayFromBase64(getInventoryString());
            return  items;
        } catch (Exception e) {
            return null;
        }
    }

    public ItemStack[] getArmor(){
        try {
            if(getArmorString() == ""){
                return null;
            }
            return InventorySerialization.itemStackArrayFromBase64(getArmorString());
        } catch (Exception e) {
            return null;
        }
    }

    public void updateInventory(Inventory inventory){
        updateInventory(InventorySerialization.toBase64(inventory));
    }

    public void updateInventory(ItemStack[] items){
        updateInventory(InventorySerialization.itemStackArrayToBase64(items));
    }

    public String getTeam(){
        return TeamStats.getTeam(getUuid());
    }

    public OfflineVaroPlayer getMate(){
        return new OfflineVaroPlayer(TeamStats.getMate(getUuid()));
    }




}
