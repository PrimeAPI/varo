package de.primeapi.varo.utils;

import de.primeapi.varo.Varo;
import de.primeapi.varo.config.Conf;
import de.primeapi.varo.managers.LocationManager;
import de.primeapi.varo.sql.SQLUtils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.omg.PortableInterceptor.LOCATION_FORWARD;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {

    public static boolean isPremiumTime(){

        if(!Conf.cfg.getBoolean("settings.session.premiumsession.use")) return false;

        Date date = new Date(System.currentTimeMillis());
        DateFormat df = new SimpleDateFormat("HH");

        int h = Integer.valueOf(df.format(date));

        if( Data.primesessiondown <= h && h <= Data.primesessionup){
            return true;
        }

        return false;
    }

    public static List<OfflineVaroPlayer> getWin(){
        List<OfflineVaroPlayer> alive = SQLUtils.getAlivePlayers();


        if(alive.size() == 0){
            return new ArrayList<OfflineVaroPlayer>();
        }

        if(alive.size() <= 2){
            List<OfflineVaroPlayer> list = new ArrayList<>();
            list.add(alive.get(0));
            list.add(alive.get(0).getMate());
            return list;
        }else {
            return null;
        }
    }

    public static String getRemainingTimeShort(Long end) {
        String msg = "";

        long now = System.currentTimeMillis();
        long diff = end - now;
        long seconds = (diff / 1000);

        if (seconds >= 60 * 60 * 24) {
            long days = seconds / (60 * 60 * 24);
            seconds = seconds % (60 * 60 * 24);

            if (days <= 1) {
                msg += days + " Tag ";
            } else {
                msg += days + " Tage ";
            }

        }
        if (seconds >= 60 * 60) {
            long h = seconds / (60 * 60);
            seconds = seconds % (60 * 60);


            if(h != 0){
                h++;
            }

            if (h <= 1) {
                msg += h + " Stunde ";
            } else {
                msg += h + " Stunden ";
            }

        }
        if (msg == "") {
            if (seconds >= 60) {
                long min = seconds / 60;
                seconds = seconds % 60;

                if (min <= 1) {
                    msg += min + " Minute ";
                } else {
                    msg += min + " Minuten ";
                }
            }
        }

        return msg;
    }

    public static String getRemainingTime(Long end) {
        String msg = "";

        long now = System.currentTimeMillis();
        long diff = end - now;
        long seconds = (diff / 1000);

        if (seconds >= 60 * 60 * 24) {
            long days = seconds / (60 * 60 * 24);
            seconds = seconds % (60 * 60 * 24);

            if (days <= 1) {
                msg += days + " Tag ";
            } else {
                msg += days + " Tage ";
            }

        }
        if (seconds >= 60 * 60) {
            long h = seconds / (60 * 60);
            seconds = seconds % (60 * 60);

            if (h <= 1) {
                msg += h + " Stunde ";
            } else {
                msg += h + " Stunden ";
            }

        }
        if (seconds >= 60) {
            long min = seconds / 60;
            seconds = seconds % 60;

            if (min <= 1) {
                msg += min + " Minute ";
            } else {
                msg += min + " Minuten ";
            }
        }

        return msg;
    }


    public static Integer getLowestSession(OfflineVaroPlayer p){

        int i = 1;
        Long s1 = p.getSession1();
        Long s2 = p.getSession2();
        Long s3 = p.getSession3();

        if (s1 <= s2 && s1 <= s3) {
            i = 1;
        }
        if (s2 <= s1 && s2 <= s3) {
            i = 2;
        }
        if (s3 <= s2 && s3 <= s1) {
            i = 3;
        }


        return i;
    }

    public static Long getLowestSessionValue(OfflineVaroPlayer p, int session){
        if(session == 1){
            return p.getSession1();
        }
        if(session == 2){
            return p.getSession2();
        }
        if(session == 3){
            return p.getSession3();
        }
        return null;
    }
    public static Long getLowestSessionValue(OfflineVaroPlayer p){
        return getLowestSessionValue(p,  getLowestSession(p));
    }


    public static void startSession(VaroPlayer p){
        startSession(p, false);
    }

    public static void startSession(VaroPlayer p, boolean globalstart){

        List<OfflineVaroPlayer> win = getWin();
        if(win != null){
            if(win.size() == 0){
                p.getPlayer().kickPlayer("§4§l➤ §6§lVaro §7● §cKick\n" +
                        "\n" +
                        "§cVaro wurde beendet!\n" +
                        "\n" +
                        "§7Varo-Projekt: §e" + Data.domain + "\n" +
                        "§aSponsored by§7: §e" + Data.sponsor);
                return;
            }else {
                p.getPlayer().kickPlayer("§4§l➤ §6§lVaro §7● §cKick\n" +
                        "\n" +
                        "§cVaro ist geendet!\n" +
                        "§cSieger: " + win.get(0).getName() + " und " + win.get(1).getName() + "\n" +
                        "\n" +
                        "§7Varo-Projekt: §e" + Data.domain + "\n" +
                        "§aSponsored by§7: §e" + Data.sponsor);
                return;
            }
        }

        if(!Conf.cfg.getBoolean("settings.started")){
            try {
                p.thePlayer().setGameMode(GameMode.ADVENTURE);
                if (!Data.gotTeleported.containsKey(p.getUuid())) {
                    int i = SQLUtils.getAlivePlayers().indexOf(p.getUuid()) + 2;

                    Location location = LocationManager.getLocation(String.valueOf(i));
                    p.thePlayer().teleport(location);
                    Data.gotTeleported.put(p.getUuid(), location);
                    p.updateLocation(location);
                } else {
                    p.thePlayer().teleport(Data.gotTeleported.get(p.getUuid()));
                }
                return;
            }catch (Exception ex){
                p.getPlayer().kickPlayer("§4§l➤ §6§lVaro §7● §cKick\n" +
                        "\n" +
                        "§cBitte einen Admin mehr Spawn-Punkte einzurichten!\n" +
                        "\n" +
                        "§7Varo-Projekt: §e" + Data.domain + "\n" +
                        "§aSponsored by§7: §e" + Data.sponsor);
                return;
            }
        }

        if(p.getTeam() == null){
            p.getPlayer().kickPlayer("§4§l➤ §6§lVaro §7● §cKick\n" +
                    "\n" +
                    "§cBitte einen Admin dich zu registrieren!\n" +
                    "\n" +
                    "§7Varo-Projekt: §e" + Data.domain + "\n" +
                    "§aSponsored by§7: §e" + Data.sponsor);
            return;
        }


        if(p.getHealth() == 0){
            p.getPlayer().kickPlayer("§4§l➤ §6§lVaro §7● §cAusschluss\n" +
                    "\n" +
                    "§cDu bist §c§ngestorben§c!\n" +
                    "\n" +
                    "§eVielen dank fürs mitspielen" + "\n" +
                    "\n" +
                    "§7Varo-Projekt: §e" + Data.domain + "\n" +
                    "§aSponsored by§7: §e" + Data.sponsor);
            return;
        }

        if(p.getStrikes().size() >= Data.maxStrikes){
            p.getPlayer().kickPlayer("§4§l➤ §6§lVaro §7● §cAusschluss\n" +
                    "\n" +
                    "§cDu hast zu viele §c§nStrikes§c!\n" +
                    "\n" +
                    "§eVielen dank fürs mitspielen" + "\n" +
                    "\n" +
                    "§7Varo-Projekt: §e" + Data.domain + "\n" +
                    "§aSponsored by§7: §e" + Data.sponsor);
            return;
        }

        if(getLowestSessionValue(p) > System.currentTimeMillis()){
            p.getPlayer().kickPlayer("§4§l➤ §6§lVaro §7● §cKick\n" +
                    "\n" +
                    "§cDu hast deine Sessions aufgebraucht!\n" +
                    "§7Warte noch: §e" + getRemainingTime(getLowestSessionValue(p)) + "\n" +
                    "\n" +
                    "§7Varo-Projekt: §e" + Data.domain + "\n" +
                    "§aSponsored by§7: §e" + Data.sponsor);
            return;
        }

        if(Utils.isPremiumTime()){
            Data.sessioning.put(p.getUuid(), Data.sessionPlusLength);
        }else {
            Data.sessioning.put(p.getUuid(), Data.sessionLength);
        }
        for(Player all : Bukkit.getOnlinePlayers()){
            all.showPlayer(p.getPlayer());
            if(!globalstart) {
                all.sendMessage(Data.pr + " §7[§3" + p.getTeam() + "§7] §b" + p.getName() + "§7 hat das Spiel betreten!");
            }
        }

        if(!globalstart) {
            Data.frozen.add(p.getUuid());
        }


        if(globalstart){
            for(World world : Bukkit.getWorlds()){
                world.getWorldBorder().setCenter(new Location(world, 0, 0, 0));
                world.getWorldBorder().setSize(Data.boarderStart);
            }
        }

        if(p.getLocation() != null) {
            p.getPlayer().teleport(p.getLocation());
        }else {
            int i = SQLUtils.getAliveUUIDS().indexOf(p.getUuid()) + 1;

            Location location = LocationManager.getLocation(String.valueOf(i));
            p.thePlayer().teleport(location);
        }
        if(p.getInventory() == null){
            p.thePlayer().getInventory().clear();
        }else {
            p.getPlayer().getInventory().setContents(p.getInventory());
        }
        p.getPlayer().setHealth(p.getHealth());
        p.updateName(p.getPlayer().getName());
        p.thePlayer().setGameMode(GameMode.SURVIVAL);
        p.updatelastJoin(System.currentTimeMillis());
        if(!globalstart) {
            p.sM(" Du bist nun für 10 Sekunden geschützt!");
        }

        int sess = Utils.getLowestSession(p);
        if(sess == 1){
            p.updateSession1(System.currentTimeMillis() + Data.sessionWait);
        }
        if(sess == 2){
            p.updateSession2(System.currentTimeMillis() + Data.sessionWait);
        }
        if(sess == 3){
            p.updateSession3(System.currentTimeMillis() + Data.sessionWait);
        }
        if(!globalstart) {
            Bukkit.getScheduler().scheduleSyncDelayedTask(Varo.getInstance(), () -> {
                Data.frozen.remove(p.getUuid());
                p.sM(" Du kannst dich nun bewegen!");
            }, 10 * 20L);
        }

    }


    public static String getDate(Long ms) {
        try {
            Date date = new Date(ms);
            DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
            return df.format(date);
        } catch (Exception ex) {
            return "Fehler: 2";
        }
    }

}
