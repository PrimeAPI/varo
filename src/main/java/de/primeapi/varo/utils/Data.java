package de.primeapi.varo.utils;

import de.primeapi.varo.config.Conf;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class Data {

    public static final boolean debug = true;

    public static final String pr = Conf.getString("settings.prefix.normal");
    public static final String prError = Conf.getString("settings.prefix.error");
    public static final String prSuccess = Conf.getString("settings.prefix.success");

    public static final String sponsor = Conf.getString("settings.sponsor");
    public static final String domain = Conf.getString("settings.ip");

    public static final int sessionLength = Conf.cfg.getInt("settings.session.lenghtInSecconds");
    public static final int sessionPlusLength = Conf.cfg.getInt("settings.session.premiumsession.lenghtInSecconds");

    public static final int primesessiondown = Conf.cfg.getInt("settings.session.premiumsession.lowerBoarder");
    public static final int primesessionup = Conf.cfg.getInt("settings.session.premiumsession.upperBoarder");

    public static final int maxStrikes = Conf.cfg.getInt("settings.strikes.autoban");
    public static final int cordLeak = Conf.cfg.getInt("settings.strikes.leakCoordinates");
    public static final int invClear = Conf.cfg.getInt("settings.strikes.clearInventory");

    public static boolean started = false;

    public static final int boarderStart = Conf.cfg.getInt("settings.worldboarder.size");
    public static final int boarderReduce = Conf.cfg.getInt("settings.worldboarder.reduce.amount");

    public static int cd = 30;

    public static final Long sessionWait = Conf.cfg.getLong("settings.session.wait");

    public static HashMap<UUID, Integer> sessioning = new HashMap<>();

    public static ArrayList<UUID> frozen = new ArrayList<>();

    public static ArrayList<UUID> vanished = new ArrayList<>();

    public static HashMap<UUID, Location> gotTeleported = new HashMap<>();

    public static HashMap<UUID, UUID> invsee = new HashMap<>();




}
