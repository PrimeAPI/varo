package de.primeapi.varo.utils;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public class InventoryBuilder {

    private String invName;
    private int invSize;
    private InventoryHolder invHolder;
    private Map<Integer, ItemStack> invItems = new HashMap<>();
    private ItemStack fillerItem;
    private boolean useFiller = false;

    public InventoryBuilder setName(String name) {
        invName = name;
        return this;
    }

    public InventoryBuilder setSize(int size) {
        invSize = size;
        return this;
    }

    public InventoryBuilder setItem(int slot, ItemStack item) {
        invItems.put(slot, item);
        return this;
    }

    public InventoryBuilder setHolder(InventoryHolder holder) {
        invHolder = holder;
        return this;
    }

    public InventoryBuilder useFiller() {
        useFiller = true;
        return this;
    }

    public InventoryBuilder setFiller(ItemStack filler) {
        fillerItem = filler;
        return this;
    }

    public Inventory build() {
        Inventory inv = Bukkit.createInventory(invHolder, invSize, invName);
        for (int slot : invItems.keySet()) {
            inv.setItem(slot, invItems.get(slot));
        }
        if (useFiller) {
            if (fillerItem != null) {
                for (int slot = 0; slot < invSize; slot++) {
                    if (inv.getItem(slot) == null) {
                        inv.setItem(slot, fillerItem);
                    }
                }
            } else {
                throw new NullPointerException("Filler can not be null when using filler!");
            }
        }

        return inv;
    }
}
