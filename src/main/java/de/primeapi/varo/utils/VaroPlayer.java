package de.primeapi.varo.utils;

import de.primeapi.varo.sql.SQLPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public class VaroPlayer extends OfflineVaroPlayer {

    Player p;

    public VaroPlayer(UUID uuid) {
        super(uuid);
        p = Bukkit.getPlayer(uuid);
    }

    public VaroPlayer(String name) {
        super(name);
        p = Bukkit.getPlayer(name);
    }

    public VaroPlayer(String name, UUID uuid) {
        super(name, uuid);
    }

    public VaroPlayer(Player p){
        super(p.getName(), p.getUniqueId());
        this.p = p;
        updateName(p.getName());
    }

    public Player getPlayer() {
        return p;
    }
    public Player thePlayer() {
        return p;
    }

    public boolean hasPermission(String s){
        return p.hasPermission(s);
    }

    public void sendNoPerm(){
        sendError("§c Keine Berechtigung!");
    }



    public void setHealth(Double d){
        p.setHealth(d);
        updateHealth(d);
    }



    public void sendMessage(String s){
        p.sendMessage(Data.pr + s);
    }
    public void sM(String s){
        p.sendMessage(Data.pr + s);
    }
    public void sendError(String s){
        p.sendMessage(Data.prError + s);
    }
    public void sendDebug(String s){ if(Data.debug) p.sendMessage(Data.pr + s); }
    public void sendSuccess(String s){
        p.sendMessage(Data.prSuccess + s);
    }
    public String getRemainingSession(){
        int sec = Data.sessioning.get(getUuid());
        int min = sec / 60;
        sec -= min * 60;
        return min + ":" + sec;
    }

}
