package de.primeapi.varo.utils;


import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class ItemBuilder {


    //vars
    Material material;
    int materialInt;
    int amount = 1;
    short damage = 0;
    Byte subid = 0;
    List<String> lore = new LinkedList<>();
    String name = "";
    boolean lether = false;
    Color color;
    boolean skull = false;
    String skullOwner;
    HashMap<Enchantment, Integer> enchantments = new HashMap<>();
    List<PotionType> potions = new ArrayList<>();
    ItemStack itemStack;


    //builder
    public ItemBuilder(Material material) {
        this.material = material;
    }

    public ItemBuilder(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public ItemBuilder(Material material, Byte subid) {
        this.material = material;
        this.subid = subid;
    }

    public ItemBuilder(int material) {
        this.materialInt = material;
    }

    public ItemBuilder(int material, Byte subid) {
        this.materialInt = material;
        this.subid = subid;
    }

    //methods

    public ItemBuilder setDamage(int damage) {
        this.damage = (short) damage;
        return this;
    }

    public ItemBuilder setLore(List<String> lore) {
        this.lore = lore;
        return this;
    }


    public ItemBuilder addEnchant(Enchantment enchantment, Integer i){
        enchantments.put(enchantment, i);
        return this;
    }

    public ItemBuilder addLore(String s) {
        List<String> lore = this.lore;


        lore.add(s);

        this.lore = lore;
        return this;
    }

    public ItemBuilder addPotion(PotionType p){
        potions.add(p);
        return this;
    }

    public ItemBuilder setDisplayName(String s) {
        this.name = s;
        return this;
    }

    public ItemBuilder setLeatherColor(Color color) {
        this.lether = true;
        this.color = color;
        return this;
    }

    public ItemBuilder setSkullOwner(String name) {
        this.skull = true;
        this.skullOwner = name;
        return this;
    }

    public ItemBuilder setAmount(int amount){
        this.amount = amount;
        return this;
    }


    //ItemBuiler
    public ItemStack build() {
        ItemStack itemStack = null;
        if (this.itemStack != null) {
            itemStack = this.itemStack;
        } else {
            if (this.material == null) {
                itemStack = new ItemStack(this.materialInt, this.amount, this.damage, this.subid);
            } else {
                itemStack = new ItemStack(this.material, this.amount, this.damage, this.subid);
            }
        }
            ItemMeta itemMeta = itemStack.getItemMeta();
            itemMeta.setDisplayName(this.name);
            itemMeta.setLore(this.lore);

            for (Enchantment enchantment : enchantments.keySet()) {
                itemMeta.addEnchant(enchantment, enchantments.get(enchantment), true);
            }

            itemStack.setItemMeta(itemMeta);

            if (this.lether) {
                LeatherArmorMeta leatherArmorMeta = (LeatherArmorMeta) itemStack.getItemMeta();
                leatherArmorMeta.setColor(this.color);
                itemStack.setItemMeta(leatherArmorMeta);
                return itemStack;
            }

            if (this.potions.size() != 0) {
                PotionMeta potionMeta = (PotionMeta) itemStack.getItemMeta();

            }

            if (this.skull) {
                SkullMeta skullMeta = (SkullMeta) itemStack.getItemMeta();
                skullMeta.setOwner(this.skullOwner);
                itemStack.setItemMeta(skullMeta);
                return itemStack;
            }
            return itemStack;
    }


}